# BaseJupyterGitIntegration

This repository is a base repository to facilitate the integration of jupyter notebook files and git.

It uses jq to parse the Jupyter notebooks and remove their output before committing.

This behavior is enforced by using git filters on all *.ipynb.



## How to install:

1. Do **NOT** clone this repository, download it

   1. If there is a .git folder, remove it

2. Make a new repository and put all the downloaded files in it

3. Edit ./.gitconfig in the local folder and change **jq-win64.exe** to **jq-linux64** if you are under Linux

4. do a :

```sh
git config --local include.path ../.gitconfig
```

   This will add our custom ./.gitconfig to the repository configuration

5. Add all the files to your repository, commit and push

## How to use:

1. Start Jupyter in the repository folder

```sh
jupyter notebook
```

2. Make a new notebook 

3. Use it as usual

4. Don't forget to save the notebook in Jupyter

5. simply add the notebook files, commit and push. The output are automagically removed before committing. This will **not** modify your local copy

## Problems:

from the source: [Making git and Jupyter notebooks play nice](http://timstaley.co.uk/posts/making-git-and-jupyter-notebooks-play-nice/)

Note that we're into git-powertool territory here, so things might be a little less polished compared to the (*cough*) usual intuitive git interface you're used to.

To start off with, assuming a pre-existing set of notebooks, you'll want to add a 'do-nothing' commit, where you simply pull in the newly-filtered versions of your notebooks and trim out any unwanted metadata. Just `git add` your notebooks, noting that you may need to `touch` them first, so git picks up on the timestamp-modification and actually looks at the files for changes. Then,

```
git diff --cached

```

to see the patch removing all the cruft. Commit that, then go ahead, run your notebooks, leave uncleaned outputs all over the place. Unless you change the actual code-cell contents, your git diff should be blank!

Great. Except. If you have executed a notebook since your last commit, `git status` may show that file as 'modified', despite the fact that when you `git diff`, the filters go into action and no differences-to-HEAD are found. So you have to 'tune out' these false-positive modified flags when reading the git-status. Another issue is that if you use a diff-GUI such as [meld](http://meldmerge.org/), then beware: unlike `git diff`, `git difftool` will **not** apply filters to the working directory before comparing with the repo HEAD - so your command-line and GUI diffs have suddenly diverged! The logic behind this difference in behaviour is that GUI programs give the option to edit the local working-copy directly, as discussed at length in [this thread](http://git.661346.n2.nabble.com/Using-clean-smudge-filters-with-difftool-td7633427.html). This has clearly [caught out others before](http://www.softec.lu/site/DevelopersCorner/GitSmudgeCleanCorrupted).

If they bother you, these false-positives and diff-divergences can easily be resolved by manually applying the jq-filters before you run your diffs. For convenience, my *~/.bashrc* also defines the following command to apply the filters to all notebooks in the current working directory:

```sh
alias nbstrip_jq="jq --indent 1 \
    '(.cells[] | select(has(\"outputs\")) | .outputs) = []  \
    | (.cells[] | select(has(\"execution_count\")) | .execution_count) = null  \
    | .metadata = {\"language_info\": {\"name\": \"python\", \"pygments_lexer\": \"ipython3\"}} \
    | .cells[].metadata = {} \
    '"
function nbstrip_all_cwd {
    for nbfile in *.ipynb; do
        echo "$( nbstrip_jq $nbfile )" > $nbfile
    done
    unset nbfile
}
```

*Note*: jq must be changed or installed at the system level.

Addtionally, let me note that **clean/smudge filters often do not play well with rebase operations**. Things get very confusing if you try to rebase across commits before / after applying a clean-filter. The simplest way to work around this is to simply comment out the relevant filter-assignment line in *.gitattributes_global* while performing a rebase, then uncomment it when done.

As a parting note, if you also choose to configure your gitattributes globally, you may want to know how to 'whitelist' notebooks in a particular repository (for example, if you're checking-in executed notebooks to a github-pages documentation branch). This is dead easy, just add a local *.gitattributes* file to the repository and 'unset' the filter attribute, like so:

```
*.ipynb -filter
```

Or you could replace the `*.ipynb` with a path to a specific notebook, etc.



## Original source:

[Making git and Jupyter notebooks play nice](http://timstaley.co.uk/posts/making-git-and-jupyter-notebooks-play-nice/)